<?php /*
3) Em um próximo acesso às páginas do sistema, mostre automaticamente qual foi
o nome do último filme visitado e a hora desta visita.
*/
include('utils/functions.php');

$filmes = array(
    array(
        "slug"=>"eden",
        "titulo"=>"Eden",
        "ano"=>"2014",
        "pais"=>"EUA",
        "poster"=>array(
            "arquivo"=>"eden.jpg",
            "alt"=>""
        )
    ),
    array(
        "slug"=>"hateful_eight",
        "titulo"=>"Hateful Eight",
        "ano"=>"2014",
        "pais"=>"EUA",
        "poster"=>array(
            "arquivo"=>"hateful_eight.jpg",
            "alt"=>""
        )
    ),
    array(
        "slug"=>"ex_machina",
        "titulo"=>"EX-Machina",
        "ano"=>"2014",
        "pais"=>"EUA",
        "poster"=>array(
            "arquivo"=>"ex_machina.jpg",
            "alt"=>""
        )
    ),
    array(
        "slug"=>"lost_river",
        "titulo"=>"Lost River",
        "ano"=>"2014",
        "pais"=>"EUA",
        "poster"=>array(
            "arquivo"=>"lost_river.jpg",
            "alt"=>""
        )
    ),
    array(
        "slug"=>"the_martian",
        "titulo"=>"The Martian",
        "ano"=>"2014",
        "pais"=>"EUA",
        "poster"=>array(
            "arquivo"=>"the_martian.jpg",
            "alt"=>""
        )
    )
);
$filme = false;

include ('partials/head.php');
?>

<section class="content">
    <?php include('partials/topo.php') ?>

    <ul class="filmes__lista">
        <?php foreach($filmes as $filme) { ?>
        <li class="filmes__item">
            <article class="filme">
                <a class="filme__link"
                href="detalhes.php?filme=<?php echo $filme["slug"]; ?>">
                    <img class="filme__imagem"
                    alt="<?php echo $filme["poster"]["alt"]; ?>"
                    src="filmes/images/<?php echo $filme["slug"]; ?>/poster/<?php echo $filme["poster"]["arquivo"] ?>" />

                    <div class="filme__detalhes">
                        <div class="filme__detalhes__conteudo">
                            <h3 class="filme__detalhes__titulo"><?php echo $filme["titulo"]; ?></h3>
                            <p class="filme__detalhes__ano"><?php echo $filme["ano"]; ?></p>
                            <span class="filme__detalhes__pais flag-us"><?php echo $filme["pais"]; ?></span>
                        </div>
                    </div>
                </a>
            </article>
        </li>
        <?php } ?>
    </ul>

    <?php include ('partials/footer.php'); ?>
</section>

</body>
</html>
