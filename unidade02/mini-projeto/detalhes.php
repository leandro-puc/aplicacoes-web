<?php
/*
3) Em um próximo acesso às páginas do sistema, mostre automaticamente qual foi
o nome do último filme visitado e a hora desta visita.
*/
include('utils/db.php');
include('utils/functions.php');
salvaFilme($filme["slug"]);
include ('partials/head.php');
?>

<section class="content">
    <header class="content__header">
        <h2>
            Filmes
            <small>/ <?php echo $filme["titulo"]; ?></small>
        </h2>

        <div class="content__header__opcoes">
            <a href="index.php">
                &larr;
                Voltar
            </a>

            <button title="Configurações"
            type="button">
                <svg aria-hidden="true">
                    <use xlink:href="assets/img/sprite.svg#cog"  />
                </svg>
            </button>
        </div>
    </header>

    <article class="filme__post">
        <header class="filme__post__cabecalho">
            <h3>
                <?php echo $filme["titulo"]; ?> (<?php echo $filme["ano"]; ?>)
                <small><?php echo $filme["tituloOriginal"]; ?></small>
            </h3>

            <div class="filme__meta">
                <ul class="filme__meta__detalhes">
                    <li title="País"><?php echo $filme["pais"]; ?></li>
                    <li title="Genêro(s)">
                        <?php foreach ($filme["genero"] as $genero) {
                            echo $genero;

                            if($genero !== end($filme["genero"])) {
                                echo ', ';
                            }
                        } ?>
                    </li>
                    <li title="Faixa etária"><?php echo $filme["faixaEtaria"]; ?> anos</li>
                </ul>

                <ul class="filme__meta__direcao">
                <?php foreach($filme["direcao"] as $diretor) { ?>
                    <li><?php echo $diretor; ?></li>
                <?php } ?>
                </ul>
            </div>
        </header>

        <figure class="filme__poster">
            <img alt="<?php echo $filme["poster"]["alt"]; ?>"
            src="filmes/images/<?php echo $filme["slug"]; ?>/poster/<?php echo $filme["poster"]["arquivo"]; ?>" />

            <figcaption>Poster de <?php echo $filme["titulo"]; ?></figcaption>
        </figure>

        <div class="filme__post__detalhes">
            <section>
                <h3>Sinopse</h3>

                <p><?php echo $filme["sinopse"]; ?></p>
            </section>

            <section>
                <h3>Elenco</h3>

                <ul class="filme__elenco">
                    <?php foreach ($filme["elenco"] as $ator) { ?>
                    <li class="filme__elenco__ator">
                        <figure class="filme__ator">
                            <img alt="<?php echo $ator["alt"]; ?>"
                            src="filmes/images/atores/<?php echo $ator["foto"]; ?>" />

                            <figcaption>
                                <strong><?php echo $ator["nome"]; ?></strong>
                                <?php echo $ator["papel"]; ?>
                            </figcaption>
                        </figure>
                    </li>
                    <?php } ?>
                </ul>
            </section>

            <section>
                <h3>Galeria</h3>

                <ul class="filme__galeria">
                    <?php foreach($filme["imagens"] as $imagem) { ?>
                    <li class="filme__galeria__imagem">
                        <figure class="filme__imagem">
                            <img alt="<?php echo $imagem["alt"]; ?>"
                            src="filmes/images/<?php echo $filme["slug"]; ?>/galeria/<?php echo $imagem["arquivo"]; ?>" />

                            <figure><?php echo $imagem["descricao"]; ?></figure>
                        </figure>
                    </li>
                    <?php } ?>
                </ul>
            </section>

            <section>
                <h3>Trailer</h3>

                <div class="filme__trailer">
                    <iframe src="https://www.youtube.com/embed/<?php echo videoYoutube($filme["trailer"]); ?>"
                    width="560" height="315"  frameborder="0" allowfullscreen></iframe>
                </div>
            </section>
        </div>

        <?php include ('partials/footer.php'); ?>
    </article>
</section>

</body>
</html>
