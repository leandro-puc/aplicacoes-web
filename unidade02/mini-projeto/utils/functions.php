<?php

// receber id do video do youtube
function videoYoutube($link) {
    $posicaoID = strpos($link, "v=");
    $idVideo = substr_replace($link, '', 0, $posicaoID+2);

    return $idVideo;
}

function diasExpiracao($quantidadeDeDias) {
    $dia = 60*60*24;
    return time()+($quantidadeDeDias*$dia);
}

function salvaFilme($filme) {
    $dataAcesso = date('d/m/Y') . '_as_' . date('H:i') . 'h';
    return setcookie('ultimaVisita', $filme . ',' . $dataAcesso, diasExpiracao(30));
}

function removeCookie($cookie) {
    return setcookie($cookie, 'x', 1);
}