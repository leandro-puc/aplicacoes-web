<?php
// true - dados estaticos de teste
// false - dados do banco de dados
$teste = true;

// se for ambiente de teste
if($teste) {
    if( isset($_GET["filme"]) ) {
        $link = $_GET["filme"];
    }

    switch($link) {
        case 'eden' :
            include('teste/eden.php');
            break;

        case 'ex_machina' :
            include('teste/ex_machina.php');
            break;

        case 'hateful_eight' :
            include('teste/hateful_eight.php');
            break;

        case 'kingsman_the_secret_service' :
            include('teste/kingsman_the_secret_service.php');
            break;

        case 'lost_river' :
            include('teste/lost_river.php');
            break;

        case 'the_martian' :
            include('teste/the_martian.php');
            break;

        default :
            include('teste/eden.php');
            break;
    }
// se nao for ambiente de teste
} else {
    var_dump($teste);

    exit;
}