<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>
        <?php if($filme) { echo $filme["titulo"] . ' (' . $filme["ano"]. ')'; } else { echo 'Lista de Filmes'; } ?>
        -
        Filmepédia
    </title>

    <link rel="stylesheet" href="assets/css/style.css" />
</head>
<body>