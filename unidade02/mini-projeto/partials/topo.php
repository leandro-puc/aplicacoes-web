
<header class="content__header">
    <h2>Filmes</h2>

    <div class="content__header__opcoes">
        <a title="Remover cookies"
        href="utils/removeCookie.php">
            <svg aria-hidden="true"><use xlink:href="assets/img/sprite.svg#cog" /></svg>
        </a>
    </div>
</header>
