<?php
$filme = array(
    "slug"=>"lost_river",
    "titulo"=>"Rio Perdido",
    "tituloOriginal"=>"Lost River",
    "ano"=>"2014",
    "pais"=>"EUA",
    "genero"=>array(
        "Drama",
        "Fantasia",
        "Mistério"
    ),
    "faixaEtaria"=>"12",
    "direcao"=>array(
        "Ryan Gosling"
    ),
    "elenco"=>array(
        array(
            "nome"=>"Christina Hendricks",
            "papel"=>"Billy",
            "foto"=>"christina_hendricks.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"Iain De Caestecker",
            "papel"=>"Bones",
            "foto"=>"iain_de_caestecker.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"Saoirse Ronan",
            "papel"=>"Rat",
            "foto"=>"saoirse_ronan.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"Matt Smith",
            "papel"=>"Bully",
            "foto"=>"matt_smith.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"Ben Mendelsohn",
            "papel"=>"Dave",
            "foto"=>"ben_mendelsohn.jpg",
            "alt"=>""
        )
    ),
    "poster"=>array(
        "arquivo"=>"lost_river.jpg",
        "alt"=>""
    ),
    "imagens"=>array(
        array(
            "arquivo"=>"image_1.jpg",
            "alt"=>"",
            "descricao"=>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est, veniam."
        ),
        array(
            "arquivo"=>"image_2.jpg",
            "alt"=>"",
            "descricao"=>"Lorem ipsum dolor sit amet."
        ),
        array(
            "arquivo"=>"image_3.jpg",
            "alt"=>"",
            "descricao"=>"Lorem ipsum dolor sit amet, consectetur adipisicing."
        )
    ),
    "trailer"=>"https://www.youtube.com/watch?v=H8ngDiG9V8w",
    "sinopse"=>"Quando a cidade em que mora está prestes a desaparecer, Billy (Christina Hendricks), mãe de dois filhos, envolve-se em um submundo fantástico e macabro, enquanto seu filho adolescente descobre uma estrada secreta que leva a uma cidade subaquática. Tanto Billy quanto seu filho devem mergulhar neste mistério, se quiserem sobreviver."
);