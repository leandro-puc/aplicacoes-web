<?php
$filme = array(
    "slug"=>"hateful_eight",
    "titulo"=>"Os Oito Odiados",
    "tituloOriginal"=>"The Hateful Eight",
    "ano"=>"2015",
    "pais"=>"EUA",
    "genero"=>array(
        "Crime",
        "Drama",
        "Mistério"
    ),
    "faixaEtaria"=>"18",
    "direcao"=>array(
        "Quentin Tarantino"
    ),
    "elenco"=>array(
        array(
            "nome"=>"Samuel L. Jackson",
            "papel"=>"Major Marquis Warren",
            "foto"=>"samuel_l_jackson.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"Kurt Russell",
            "papel"=>"John Ruth",
            "foto"=>"kurt_russell.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"Jennifer Jason Leigh",
            "papel"=>"Daisy Domergue",
            "foto"=>"jennifer_jason_leigh.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"Walton Goggins",
            "papel"=>"Sheriff Chris Mannix",
            "foto"=>"walton_goggins.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"Demián Bichir",
            "papel"=>"Bob",
            "foto"=>"demian_bichir.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"Tim Roth",
            "papel"=>"Oswaldo Mobray",
            "foto"=>"tim_roth.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"Michael Madsen",
            "papel"=>"Joe Gage",
            "foto"=>"michael_madsen.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"Bruce Dern",
            "papel"=>"General Sandy Smithers",
            "foto"=>"bruce_dern.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"James Parks",
            "papel"=>"O.B.",
            "foto"=>"james_parks.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"Zoë Bell",
            "papel"=>"Six-Horse Judyy",
            "foto"=>"zoe_bell.jpg",
            "alt"=>""
        )
    ),
    "poster"=>array(
        "arquivo"=>"hateful_eight.jpg",
        "alt"=>""
    ),
    "imagens"=>array(
        array(
            "arquivo"=>"image_1.jpg",
            "alt"=>"",
            "descricao"=>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est, veniam."
        ),
        array(
            "arquivo"=>"image_2.jpg",
            "alt"=>"",
            "descricao"=>"Lorem ipsum dolor sit amet."
        ),
        array(
            "arquivo"=>"image_3.jpg",
            "alt"=>"",
            "descricao"=>"Lorem ipsum dolor sit amet, consectetur adipisicing."
        )
    ),
    "trailer"=>"https://www.youtube.com/watch?v=6_UI1GzaWv0",
    "sinopse"=>"Durante uma nevasca, o carrasco John Ruth (Kurt Russell) está transportando uma prisioneira, a famosa Daisy Domergue (Jennifer Jason Leigh), que ele espera trocar por grande quantia de dinheiro. No caminho, os viajantes aceitam transportar o caçador de recompensas Marquis Warren (Samuel L. Jackson), que está de olho em outro tesouro, e o xerife Chris Mannix (Walton Goggins), prestes a ser empossado em sua cidade. Como as condições climáticas pioram, eles buscam abrigo no Armazém da Minnie, onde quatro outros desconhecidos estão abrigados. Aos poucos, os oito viajantes no local começam a descobrir os segredos sangrentos uns dos outros, levando a um inevitável confronto entre eles."
);