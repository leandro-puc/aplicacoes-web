<?php
$filme = array(
    "slug"=>"the_martian",
    "titulo"=>"Perdido em Marte",
    "tituloOriginal"=>"The Martian",
    "ano"=>"2015",
    "pais"=>"EUA",
    "genero"=>array(
        "Aventura",
        "Drama",
        "Ficção Científica"
    ),
    "faixaEtaria"=>"12",
    "direcao"=>array(
        "Ridley Scott"
    ),
    "elenco"=>array(
        array(
            "nome"=>"Matt Damon",
            "papel"=>"Mark Watney",
            "foto"=>"matt_damon.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"Jessica Chastain",
            "papel"=>"Melissa Lewis",
            "foto"=>"jessica_chastain.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"Kristen Wiig",
            "papel"=>"Annie Montrose",
            "foto"=>"kristen_wiig.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"Jeff Daniels",
            "papel"=>"Teddy Sanders",
            "foto"=>"jeff_daniels.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"Michael Peña",
            "papel"=>"Rick Martinez",
            "foto"=>"michael_pena.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"Sean Bean",
            "papel"=>"Mitch Henderson",
            "foto"=>"sean_bean.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"Kate Mara",
            "papel"=>"Beth Johanssen",
            "foto"=>"kate_mara.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"Sebastian Stan",
            "papel"=>"Chris Beck",
            "foto"=>"sebastian_stan.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"Aksel Hennie",
            "papel"=>"Alex Vogel",
            "foto"=>"aksel_hennie.jpg",
            "alt"=>""
        )
    ),
    "poster"=>array(
        "arquivo"=>"the_martian.jpg",
        "alt"=>""
    ),
    "imagens"=>array(
        array(
            "arquivo"=>"image_1.jpg",
            "alt"=>"",
            "descricao"=>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est, veniam."
        ),
        array(
            "arquivo"=>"image_2.jpg",
            "alt"=>"",
            "descricao"=>"Lorem ipsum dolor sit amet."
        ),
        array(
            "arquivo"=>"image_3.jpg",
            "alt"=>"",
            "descricao"=>"Lorem ipsum dolor sit amet, consectetur adipisicing."
        )
    ),
    "trailer"=>"https://www.youtube.com/watch?v=Ue4PCI0NamI",
    "sinopse"=>"O astronauta Mark Watney (Matt Damon) é enviado a uma missão em Marte. Após uma severa tempestade ele é dado como morto, abandonado pelos colegas e acorda sozinho no misterioso planeta com escassos suprimentos, sem saber como reencontrar os companheiros ou retornar à Terra."
);