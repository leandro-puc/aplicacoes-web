<?php
$filme = array(
    "slug"=>"eden",
    "titulo"=>"Eden",
    "tituloOriginal"=>"Eden",
    "ano"=>"2014",
    "pais"=>"EUA",
    "genero"=>array(
        "Drama",
        "Musical"
    ),
    "faixaEtaria"=>"16",
    "direcao"=>array(
        "Fulano de Tal",
        "Cicrano da Silva"
    ),
    "elenco"=>array(
        array(
            "nome"=>"John Hurt",
            "papel"=>"John Hurt",
            "foto"=>"john-hurt.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"John Hurt",
            "papel"=>"John Hurt",
            "foto"=>"john-hurt.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"John Hurt",
            "papel"=>"John Hurt",
            "foto"=>"john-hurt.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"John Hurt",
            "papel"=>"John Hurt",
            "foto"=>"john-hurt.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"John Hurt",
            "papel"=>"John Hurt",
            "foto"=>"john-hurt.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"John Hurt",
            "papel"=>"John Hurt",
            "foto"=>"john-hurt.jpg",
            "alt"=>""
        )
    ),
    "poster"=>array(
        "arquivo"=>"eden.jpg",
        "alt"=>""
    ),
    "imagens"=>array(
        array(
            "arquivo"=>"image_1.jpg",
            "alt"=>"",
            "descricao"=>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est, veniam."
        ),
        array(
            "arquivo"=>"image_2.jpg",
            "alt"=>"",
            "descricao"=>"Lorem ipsum dolor sit amet."
        ),
        array(
            "arquivo"=>"image_3.jpg",
            "alt"=>"",
            "descricao"=>"Lorem ipsum dolor sit amet, consectetur adipisicing."
        )
    ),
    "trailer"=>"https://www.youtube.com/watch?v=2l1T9xs-o0o",
    "sinopse"=>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni iste hic veritatis deserunt modi tempore neque assumenda nostrum fugit illo nisi maiores iusto explicabo saepe vel natus tenetur sapiente voluptatibus cupiditate, eos quibusdam sint aliquid placeat. Assumenda dolore ipsa temporibus, vero voluptatum reprehenderit aliquid, eius quis neque, nesciunt quidem nam, animi unde ipsum ab officia laudantium voluptas repellendus minima. Ipsa tenetur nihil veritatis officia, corporis a nulla consequuntur voluptatibus ut ducimus maxime, ipsum qui. Accusantium soluta suscipit, doloribus ipsam quis nam, voluptatem iure ullam praesentium illum quibusdam libero cum placeat omnis reprehenderit magnam perspiciatis sit consequatur accusamus quidem asperiores id."
);