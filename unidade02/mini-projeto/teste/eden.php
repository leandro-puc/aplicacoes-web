<?php
$filme = array(
    "slug"=>"eden",
    "titulo"=>"Eden",
    "tituloOriginal"=>"Eden",
    "ano"=>"2014",
    "pais"=>"FR",
    "genero"=>array(
        "Drama",
        "Musical"
    ),
    "faixaEtaria"=>"16",
    "direcao"=>array(
        "Mia Hansen-Løve"
    ),
    "elenco"=>array(
        array(
            "nome"=>"Félix de Givry",
            "papel"=>"Paul Vallée",
            "foto"=>"felix_de_givry.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"Pauline Etienne",
            "papel"=>"Louise",
            "foto"=>"pauline_etienne.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"Vincent Macaigne",
            "papel"=>"Arnaud",
            "foto"=>"vincent_macaigne.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"Vincent Lacoste",
            "papel"=>"Thomas Bangalter",
            "foto"=>"vincent_lacoste.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"Arnaud Azoulay",
            "papel"=>"Guy-Manuel de Homem-Christo",
            "foto"=>"arnaud_azoulay.jpg",
            "alt"=>""
        )
    ),
    "poster"=>array(
        "arquivo"=>"eden.jpg",
        "alt"=>""
    ),
    "imagens"=>array(
        array(
            "arquivo"=>"image_1.jpg",
            "alt"=>"",
            "descricao"=>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est, veniam."
        ),
        array(
            "arquivo"=>"image_2.jpg",
            "alt"=>"",
            "descricao"=>"Lorem ipsum dolor sit amet."
        ),
        array(
            "arquivo"=>"image_3.jpg",
            "alt"=>"",
            "descricao"=>"Lorem ipsum dolor sit amet, consectetur adipisicing."
        )
    ),
    "trailer"=>"https://www.youtube.com/watch?v=2l1T9xs-o0o",
    "sinopse"=>"No início dos anos 1990, a música eletrônica começou a disparar na França. Paul (Félix de Givry) é um adolescente que gosta de raves, que se popularizam em Paris. Mas ele prefere um cenário mais underground. Paul e um amigo formam uma dupla de DJ´s chamada Cheers. Eles mergulham em um mundo de ritmo e batidas, rodeados por drogas e sexo."
);