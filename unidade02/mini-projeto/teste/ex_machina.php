<?php
$filme = array(
    "slug"=>"ex_machina",
    "titulo"=>"Ex_Machina: Instinto Artificial",
    "tituloOriginal"=>" Ex Machina",
    "ano"=>"2015",
    "pais"=>"UK",
    "genero"=>array(
        "Drama",
        "Mistério",
        "Ficção científica"
    ),
    "faixaEtaria"=>"14",
    "direcao"=>array(
        "Alex Garland"
    ),
    "elenco"=>array(
        array(
            "nome"=>"Domhnall Gleeson",
            "papel"=>"Caleb",
            "foto"=>"domhnall_gleeson.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"Corey Johnson",
            "papel"=>"Jay",
            "foto"=>"corey_johnson.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"Oscar Isaac",
            "papel"=>"Nathan",
            "foto"=>"oscar_isaac.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"Alicia Vikander",
            "papel"=>"Ava",
            "foto"=>"alicia_vikander.jpg",
            "alt"=>""
        ),
        array(
            "nome"=>"Sonoya Mizuno",
            "papel"=>"Kyoko",
            "foto"=>"sonoya_mizuno.jpg",
            "alt"=>""
        )
    ),
    "poster"=>array(
        "arquivo"=>"ex_machina.jpg",
        "alt"=>""
    ),
    "imagens"=>array(
        array(
            "arquivo"=>"image_1.jpg",
            "alt"=>"",
            "descricao"=>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est, veniam."
        ),
        array(
            "arquivo"=>"image_2.jpg",
            "alt"=>"",
            "descricao"=>"Lorem ipsum dolor sit amet."
        ),
        array(
            "arquivo"=>"image_3.jpg",
            "alt"=>"",
            "descricao"=>"Lorem ipsum dolor sit amet, consectetur adipisicing."
        )
    ),
    "trailer"=>"https://www.youtube.com/watch?v=XYGzRB4Pnq8",
    "sinopse"=>"Caleb (Domhnall Gleeson), um jovem programador de computadores, ganha um concurso na empresa onde trabalha para passar uma semana na casa de Nathan Bateman (Oscar Isaac), o brilhante e recluso presidente da companhia. Após sua chegada, Caleb percebe que foi o escolhido para participar de um teste com a última criação de Nathan: Ava (Alicia Vikander), uma robô com inteligência artificial. Mas essa criatura se apresenta sofisticada e sedutora de uma forma que inguém poderia prever, complicando a situação ao ponto que Caleb não sabe mais em quem confiar."
);