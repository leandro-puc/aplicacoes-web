<?php
/*
Questão 5
Escreva um script PHP que receba login e senha de um usuário a partir de um
formulário Web. Esses dados serão usados para acesso a um sistema restrito.
Transforme-os numa consulta SQL em um banco de dados.
*/

require_once("../utils/dbconexao.php");

// retorno do formulario
$login = $_POST["usuarioLogin"];
$senha = $_POST["usuarioSenha"];

// se o retorno nao veio vazio, roda a aplicacao
if( strlen( $login ) > 0 && strlen( $senha ) > 0 ) {

    // realiza a busca do usuario no banco
    $procuraUsuario = $dbConexao->query("SELECT login, senha FROM usuario" );
    $usuarioEncontrado = $procuraUsuario->fetch_array();

    if( $usuarioEncontrado["login"] == $login && $usuarioEncontrado["senha"] == $senha ) {
        // usuario conectado
    }
}

// forca o encerramento da conexao
$dbConexao->close();
