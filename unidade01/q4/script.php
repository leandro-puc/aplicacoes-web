<?php
/*
Questão 4
Crie um script PHP que receba os dados de um formulário HTML representando um
cadastro para acesso a um sistema restrito. Este cadastro deve incluir nome
completo, email, data de nascimento e senha de acesso. A senha deve ser
verificada por meio de confirmação de digitação. Após receber os dados, o script
deve transformá-los numa cláusula SQL para inserção em banco de dados.
*/

require_once("../utils/dbconexao.php");

// retorno do formulario
$nome             = $_POST["nomeCompleto"];
$email            = $_POST["email"];
$dataNascimento   = $_POST["dataNascimento"];
$senha            = $_POST["senhaAcesso"];
$confirmacaoSenha = $_POST["confirmacaoSenha"];

// se a senha tiver sido digitada, guardar as informacoes no banco
if( strlen( $senha ) > 0 && $confirmacaoSenha == $senha ) {
    // salva as informacoes no banco
    $realizarCadastro = $dbConexao->query("INSERT INTO usuario (nome, email, dataNascimento, senha) VALUES ($nome, $email, $dataNascimento, $senha)" );

    // se as informacoes foram salvas no banco
    if($realizarCadastro) {
        echo "<h1>Bem vindo, $nome</h1>";
        echo '<p><strong>Cadastro realizado com sucesso!</strong></p>';

    // se as informacoes nao foram salvas no banco
    } else {
        echo '<h1>N&atilde;o foi poss&iacute;vel realizar o seu cadastro.</h1>';

        die('Erro: ('. $dbConexao->errno .') '. $dbConexao->error);
    }
}

// forca o encerramento da conexao
$dbConexao->close();
