<?php
/*
Questão 1
Escreva um script PHP que, dada uma frase como parâmetro, retorne todas as ocorrências de uma palavra na frase e armazene suas posições.
*/

    // retorno do formulario
    $frase   = $_POST["frase"];
    $procura = $_POST["procura"];

    // array com as palavras da frase submetida pelo usuario
    $palavras = explode(" ", $frase);

    // array com as posicoes da palavra procurada
    $posicoesPalavrasRepetidas = array_keys( $palavras, $procura );

    // somente se a palavra se repetir mais de uma vez
    if( count($posicoesPalavrasRepetidas) > 1 ) {

        echo '<p>';
        // imprime a frase, destacando a palavra repetida
        foreach( $palavras as $indice=>$palavra ) {
            if( in_array($indice, $posicoesPalavrasRepetidas) ) {
                if( end($palavras) == $palavra ) {
                    echo "<b>$palavra</b>.";
                } else {
                    echo "<b>$palavra</b> ";
                }
            } else {
                if( end($palavras) == $palavra ) {
                    echo $palavra . '.';
                } else {
                    echo $palavra . ' ';
                }
            }
        }
        echo '</p>';

        echo '<p>A palavra se repete nas posi&ccedil;&otilde;es: ' . implode( ', ', $posicoesPalavrasRepetidas ) . '</p>';

    // se a palavra procurada nao se repetir na frase
    } else {
        echo '<p><strong>A palavra procurada n&atilde;o se repete na frase.</strong></p>';
        echo '<p>Frase digitada:<br />' . $frase . '</p>';
        echo '<p>Palavra procurada:<br />' . $procura . '</p>';
    }
