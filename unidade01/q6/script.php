<?php
/*
Questão 6
Escreva um código PHP que produza um formulário HTML permitindo ao usuário
aposte em um número de 1 a 12. Neste script, adicionalmente, deve constar um
valor aleatório também entre 1 e 12 no qual o sistema “apostou” previamente.
Os dados do formulário devem ser processados por outro script PHP, o qual
sorteia um número de 1 a 12 e informa se o usuário venceu aposta, o sistema
venceu a aposta, ou se nenhum dos dois foi vencedor.
*/

$apostaUsuario = $_POST["apostaUsuario"];
$apostaSistema = $_POST["apostaSistema"];

$sorteio = rand(1, 12);

// se o usuario ganhou
if($sorteio == $apostaUsuario) {
    echo '<h1>Parab&eacute;ns! Voc&ecirc; ganhou a aposta!</h1>';

    echo '<p><b>Sua aposta:</b>' . $apostaUsuario . '</p>';
    echo '<p><b>Aposta do sistema:</b>' . $apostaSistema . '</p>';
    echo '<p><b>Resultado do sorteio:</b>' . $sorteio . '</p>';

// se o computador ganhou
} else if($sorteio == $apostaSistema) {
    echo '<h1>Que pena! O computador ganhou a aposta.</h1>';

    echo '<p><b>Sua aposta:</b>' . $apostaUsuario . '</p>';
    echo '<p><b>Aposta do sistema:</b>' . $apostaSistema . '</p>';
    echo '<p><b>Resultado do sorteio:</b>' . $sorteio . '</p>';

// se nao houve ganhadores
} else {
    echo '<h1>Vish! N&atilde;o houve ganhadores na aposta!</h1>';

    echo '<p><b>Sua aposta:</b>' . $apostaUsuario . '</p>';
    echo '<p><b>Aposta do sistema:</b>' . $apostaSistema . '</p>';
    echo '<p><b>Resultado do sorteio:</b>' . $sorteio . '</p>';
}

echo '<a href="entrada.php">Tentar novamente</a>';

setcookie("resultado", strval($sorteio), 0);

session_start();
$_SESSION['apostaUsuario'] = $apostaUsuario;
$_SESSION['apostaSistema'] = $apostaSistema;
$_SESSION['sorteio']       = $sorteio;
var_dump( $_SESSION );
