<?php
/*
Questão 6
Escreva um código PHP que produza um formulário HTML permitindo ao usuário
aposte em um número de 1 a 12. Neste script, adicionalmente, deve constar um
valor aleatório também entre 1 e 12 no qual o sistema “apostou” previamente.
Os dados do formulário devem ser processados por outro script PHP, o qual
sorteia um número de 1 a 12 e informa se o usuário venceu aposta, o sistema
venceu a aposta, ou se nenhum dos dois foi vencedor.
*/

echo '<form action="script.php" method="post"><h1>Jogo das Apostas</h1>';

// aposta do usuario
echo '<label for="apostaUsuario">N&uacute;mero da aposta (de 1 a 12):</label>';
echo '<input id="apostaUsuario" name="apostaUsuario" min="1" max="12" required type="number" />';

// input da aposta do sistema
echo '<input id="apostaSistema" name="apostaSistema" required type="hidden" value=" ' . rand(1, 12) . ' " />';

echo '<button type="submit">Apostar</button></form>';
