<?php
/*
Questão 2
Crie um array chamado “estado” para armazenar as capitais dos estados da sua
região sul Brasil, utilizando as siglas dos estados como índice. Faça uma função
que busque e exiba a capital de um estado cuja sigla foi passada por parâmetro
para a função.
*/

$estados = array(
    "AC"=>"Rio Branco",
    "AP"=>"Macap&aacute;",
    "AM"=>"Manaus",
    "PA"=>"Bel&eacute;m",
    "RO"=>"Porto Velho",
    "RR"=>"Boa Vista",
    "TO"=>"Palmas"
);

function exibeCapital($uf, $array) {
    if( array_key_exists($uf, $array) ) {
        return $array[$uf];
    }

    return 'N&atilde;o encontrado';
}

echo '<p>' . exibeCapital("AC", $estados) . '</p>'; // Rio Branco
echo '<p>' . exibeCapital("AP", $estados) . '</p>'; // Macapá
echo '<p>' . exibeCapital("AM", $estados) . '</p>'; // Manaus
echo '<p>' . exibeCapital("PA", $estados) . '</p>'; // Belem
echo '<p>' . exibeCapital("RO", $estados) . '</p>'; // Porto Velho
echo '<p>' . exibeCapital("RR", $estados) . '</p>'; // Boa Vista
echo '<p>' . exibeCapital("TO", $estados) . '</p>'; // Palmas

echo '<p>' . exibeCapital("MG", $estados) . '</p>'; // Nao encontrado
echo '<p>' . exibeCapital("SP", $estados) . '</p>'; // Nao encontrado
