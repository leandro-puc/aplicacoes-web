<?php
/*
Questão 3
Implemente uma função que receba um array chamado “alunos”. Esse array deve
conter o nome e duas notas entre 0 e 10 de vários alunos. Por meio da função,
deve ser calculada uma média das duas notas de cada aluno, que só é considerado
aprovado se a sua média for maior ou igual a 7. A função deve retornar um outro
array chamado “aprovados” somente com os nomes dos alunos aprovados e em ordem
alfabética. Dica: você pode criar um vetor de vetores em PHP.
*/

$alunos = array(
    array(
        "nome"=>"Marcelo",
        "nota1"=>10,
        "nota2"=>10
    ),
    array(
        "nome"=>"Eder",
        "nota1"=>5,
        "nota2"=>5
    ),
    array(
        "nome"=>"Rafael",
        "nota1"=>8,
        "nota2"=>6
    ),
    array(
        "nome"=>"Leandro",
        "nota1"=>8,
        "nota2"=>8
    )
);

function calculaMedia($estudantes) {
    $aprovados = array();

    foreach($estudantes as $aluno) {
        $media = ($aluno["nota1"]+$aluno["nota2"])/2;

        if( $media >= 7 ) {
            array_push($aprovados, $aluno["nome"]);
        }
    }

    if( count($aprovados) > 0 ) {
        asort($aprovados);

        return $aprovados;
    }

    return FALSE;
}

$alunosAprovados = calculaMedia($alunos);

echo '<h1>Alunos aprovados</h1>';
echo '<ul>';
foreach($alunosAprovados as $aluno) {
    echo '<li>' . $aluno . '</li>';
}
echo '</ul>';
