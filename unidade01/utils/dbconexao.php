<?php

// configuracoes do banco
$dbHost     = "";
$dbUser     = "";
$dbPassword = "";
$dbName     = "";

// conexao com o banco
$dbConexao = new mysqli($dbHost, $dbUser, $dbPassword, $dbName);

// se nao tiver conexao com o banco
if ($dbConexao->connect_error) {
    die( "Não foi possível conectar: (". $dbConexao->connect_errno . ") " . $dbConexao->connect_error );
}
